import axios from 'axios'
import FormData from 'form-data'
import _ from 'lodash'

if (!FormData.prototype.getHeaders) {
  function getBoundary() {
    let boundary = '--------------------------'
    for (var i = 0; i < 24; i++) {
      boundary += Math.floor(Math.random() * 10).toString(16)
    }
    return boundary
  }
  FormData.prototype.getHeaders = function() {
    return {
      'content-type': 'multipart/form-data; boundary=' + getBoundary()
    }
  }
}

export default class {
  constructor (apiURL) {
    this.apiURL = apiURL
    this.token = null
    this.axios = axios.create({
      baseURL: apiURL,
      timeout: 10000
    })
    if (process.env.NODE_ENV === 'development') {
      this.axios.interceptors.request.use(request => {
        console.log('Starting Request: ', request)
        return request
      })
    }
  }
  auth ({email, password}) {
    return new Promise((resolve, reject) => {
      this.axios.post('/auth/token', {
        email: email,
        password: password
      }).then(resp => {
        this.isLogin = true
        this.token = resp.data.access_token
        this.user = resp.data.user
        this.axios.defaults.headers = {
          'Authorization': `Bearer ${resp.data.access_token}`
        }
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  }

  // GET /catalogs
  getCatalogs () {
    return this.axios.get('/catalogs')
  }
  // POST /catalogs
  postCatalog ({name, description, format, status}) {
    let params = {
      name: name,
      description: description,
      format: format,
      status: status
    }
    return this.axios.post('/catalogs', params)
  }
  // DELETE /catalogs/:id
  deleteCatalogByID (id) {
    return this.axios.delete(`/catalogs/${id}`)
  }

  // GET /users
  getUsers () {
    return this.axios.get('/users')
  }
  // POST /users
  postUser ({name, email, password}) {
    let params = {
      name: name,
      email: email,
      password: password
    }
    return this.axios.post('/users', params)
  }
  // DELETE /users
  deleteUserByID (id) {
    return this.axios.delete(`/users/${id}`)
  }

  // GET  /records
  getRecords () {
    return this.axios.get('/records')
  }
  // GET  /catalogs/:id/records
  getRecordsByCatalogID (collectionID) {
    return this.axios.get(`/catalogs/${collectionID}/records`)
  }
  // GET /records/:id
  getRecordByID (id) {
    return this.axios.get(`/records/${id}`)
  }
  // POST /records
  postRecord (record) {
    return this.axios.post('/records', record)
  }
  // DELETE /records/:id
  deleteRecordByID (id) {
    return this.axios.delete(`/records/${id}`)
  }
  // PUT /catalogs/:catalogId/records/:recordId
  putRecordToCatalog (catalogId, recordId) {
    return this.axios.put(`/catalogs/${catalogId}/records/${recordId}`)
  }
  deleteRecordFromCatalog (catalogId, recordId) {
    return this.axios.delete(`/catalogs/${catalogId}/records/${recordId}`)
  }

  // GET /media/:id
  getMediumByID (id) {
    return this.axios.get(`/media/${id}`)
  }

  // POST /records/:id/media/
  postMediumToRecord (id, medium) {
    let form = new FormData()
    for (let k in medium) {
      form.append(k, medium[k])
    }
    return this.axios.post(`/records/${id}/media`, form, {
      headers: form.getHeaders()
    })
  }

  // DELETE /records/:recordId/media/:mediumId
  deleteMediumFromRecord (recordId, mediumId) {
    return this.axios.delete(`/records/${recordId}/media/${mediumId}`)
  }
}
