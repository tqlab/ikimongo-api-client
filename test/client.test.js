import API from '../index'
import fs from 'fs'

const apiURL = 'http://localhost:3000'
const validUser = {
  name: 'test',
  email: 'test@test.com',
  password: 'testtest'
}
const invalidUser = {
  email: 'hoge@hoge.hoge',
  password: 'hogehoge'
}
const loggedinClient = new API(apiURL)
const testRecord = () => {
  return  {
    name: 'test',
    longitude: 12,
    latitude: 20,
    collected_at: (new Date()).toISOString(),
    additional_data: {
      '食べもの': '肉',
      '趣味': 'ダイエット'
    }
  }
}
const invalidRecord = () => {
  return {
    name: 'test',
    longitude: 12,
    latitude: 20,
    collected_at: (new Date()).toISOString(),
    additional_data: [{
      '食べもの': '肉',
      '趣味': 'ダイエット'
    }]
  }
}

const testMedium = () => {
  return {
    file: fs.createReadStream('test/files/sample.png'),
    description: 'This is a test file.'
  }
}

const testCatalog = () => {
  return {
    name: 'hello',
    description: 'hello hello',
    format: null,
    status: 'Public'
  }
}
let testCatalogId = null
const testUser = () => {
  const localPart = Math.random().toString(36).slice(-8)
  return  {
    name: 'test',
    email: `${localPart}@test.com`,
    password: 'testtest'
  }
}

beforeAll(async (done) => {
  expect.assertions(2)
  await loggedinClient.postUser(validUser).catch(e => e)
  let resp = await loggedinClient.auth(validUser)
  expect(resp.status).toBe(200)
  resp = await loggedinClient.postCatalog(testCatalog())
  expect(resp.status).toBe(201)
  testCatalogId = resp.data._id
  done()
})
afterAll(async (done) => {
  let resp = await loggedinClient.deleteCatalogByID(testCatalogId)
  expect(resp.status).toBe(200)
  done()
})

describe('about auth', () => {
  const validClient = new API(apiURL)
  const invalidClient = new API(apiURL)
  test('login should be succeed', () => {
    return validClient.auth(validUser)
  })

  test('login should be failed', () => {
    expect.assertions(1)
    return invalidClient.auth(invalidUser)
      .catch(err => {
        expect(err.response.status).toBe(401)
      })
  })
})

describe('about catalogs', () => {
  test('can post & delete catalog', async () => {
    let resp = await loggedinClient.postCatalog(testCatalog())
    await loggedinClient.deleteCatalogByID(resp.data._id)
  })

  test('can get catalogs', () => {
    expect.assertions(1)
    return loggedinClient.getCatalogs()
      .then(resp => {
        expect(resp.data).toBeInstanceOf(Array)
      })
  })
})

describe('about users', () => {
  test('can get users', () => {
    return loggedinClient.getUsers()
  })

  test('can post & delete user', async () => {
    const client = new API(apiURL)
    let user = testUser()
    let resp = await client.postUser(user)
    await client.auth({email: user.email, password: user.password})
    await client.deleteUserByID(resp.data._id)
  })
})

describe('about records', () => {
  test('can post & delete record', async () => {
    expect.assertions(2)
    let resp = await loggedinClient.postRecord(testRecord())
    expect(resp.status).toBe(201)
    resp = await loggedinClient.deleteRecordByID(resp.data._id)
    expect(resp.status).toBe(200)
  })

  test('should be fail to post record', async () => {
    try {
      loggedinClient.postRecord(invalidRecord())
    } catch(err) {
      expect.assertions(1)
      expect(err.status).toBe(400)
    }
  })

  test('can get records', async () => {
    expect.assertions(1)
    const resp = await loggedinClient.getRecords()
    expect(resp.data).toBeInstanceOf(Array)
  })

  test('can get records of a catalog', async () => {
    expect.assertions(1)
    const resp = await loggedinClient.getRecordsByCatalogID(testCatalogId)
    expect(resp.data).toBeInstanceOf(Array)
  })

  test('can put & delete record to catalogs', async () => {
    expect.assertions(3)
    let resp = await loggedinClient.postRecord(testRecord())
    expect(resp.status).toBe(201)
    let recordId = resp.data._id
    resp = await loggedinClient.putRecordToCatalog(testCatalogId, recordId)
    expect(resp.status).toBe(200)
    resp = await loggedinClient.deleteRecordFromCatalog(testCatalogId, recordId)
    expect(resp.status).toBe(200)
  })
})

describe('about media', () => {
  test('can post & get & delete media', async () => {
    expect.assertions(5)
    let resp = await loggedinClient.postRecord(testRecord())
    expect(resp.status).toBe(201)
    let recordId = resp.data._id
    resp = await loggedinClient.postMediumToRecord(recordId, testMedium() )
    let mediumId = resp.data._id
    expect(resp.status).toBe(201)

    resp = await loggedinClient.getMediumByID(mediumId)
    expect(resp.status).toBe(200)

    resp = await loggedinClient.deleteMediumFromRecord(recordId, mediumId)
    expect(resp.status).toBe(200)

    resp = await loggedinClient.deleteRecordByID(recordId)
    expect(resp.status).toBe(200)
  })
})
